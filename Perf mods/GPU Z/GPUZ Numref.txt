/*!
  * Power. Indicating perf is limited by total power limit.
  */
  NV_GPU_PERF_POLICY_ID_SW_POWER = 1,
  /*!
  * Thermal. Indicating perf is limited by temperature limit.
  */
  NV_GPU_PERF_POLICY_ID_SW_THERMAL = 2,
  /*!
  * Reliability. Indicating perf is limited by reliability voltage.
  */
  NV_GPU_PERF_POLICY_ID_SW_RELIABILITY = 4,
  /*!
  * Operating. Indicating perf is limited by max operating voltage.
  */
  NV_GPU_PERF_POLICY_ID_SW_OPERATING = 8,
  /*!
  * Utilization. Indicating perf is limited by GPU utilization.
  */
  NV_GPU_PERF_POLICY_ID_SW_UTILIZATION = 16,