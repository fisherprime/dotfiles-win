﻿Set-Alias ~ $env:USERPROFILE

function .. { cd .. }
function ... { cd ..; cd .. }
function .... { cd .. ; cd .. ; cd .. }
function ..... { cd .. ; cd .. ; cd .. ; cd .. }
function home { cd $env:USERPROFILE }
${function:~} = { Set-Location ~ } #Different assignment implementation same result
function Edit-Hosts { Invoke-Expression "sudo $(if($env:EDITOR -ne $null)  {$env:EDITOR } else { 'notepad' }) $env:windir\system32\drivers\etc\hosts" }
