@echo off
:: No output

timeout /T 3
REM 3 sec delay before executing
start "Network Monitor" /HIGH "D:\Program Files\Fiddler\Fiddler.exe"
:: START "title" [/D path] [options] "command" [parameters]
:: Titles are optional; can be blank

timeout /T 1
start "Process Explorer" /HIGH "D:\Program Files (x86)\Sysinternals\ProcessExplorer\procexp64.exe"

timeout /T 1
start "ThrottleStop" /HIGH "D:\Program Files (x86)\Monitor\ThrottleStop\ThrottleStop.exe"

timeout /T 1
start "Clevo shit" "C:\Program Files (x86)\Hotkey\HkeyTray2.exe"

@echo:
REM Prints a newline character
set /p FirewallChoice="Enable Firewall(y/Random key): "
if %FirewallChoice% == Y (
	echo Starting Firewall...
	start "Firewall" /HIGH "D:\Program Files\Oracle\VirtualBox\VBoxManage.exe" startvm "Firewall" --type headless
) else if %FirewallChoice% == y (
	echo Starting Firewall...
	start "Firewall" /HIGH "D:\Program Files\Oracle\VirtualBox\VBoxManage.exe" startvm "Firewall" --type headless
) else (
	echo Firewall off
)

@echo:
set /p SyncthingChoice="Start Syncthing(y/Random key): "
if %SyncthingChoice% == Y (
	echo Starting Syncthing...
	start "Syncthing" /NORMAL syncthing
) else if %SyncthingChoice% == y (
	echo Starting Syncthing...
	start "Syncthing" /NORMAL syncthing
) else (
	echo Syncthing off
)